CC=gcc
CFLAGS=-O2 -Wall
DIRS=bin build

all: dirs bin/dfs bin/bfs

dirs:
	mkdir -p $(DIRS)

.PHONY: dirs


bin/dfs: build/dfs.o
	$(CC) build/dfs.o -o bin/dfs

build/dfs.o:
	$(CC) $(CFLAGS) -c dfs/dfs.c -o build/dfs.o

bin/bfs: build/bfs.o
	$(CC) build/bfs.o -o bin/bfs

build/bfs.o:
	$(CC) $(CFLAGS) -c bfs/bfs.c -o build/bfs.o

clean:
	rm -rf $(DIRS)

