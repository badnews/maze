# Maze path finding code assessment #
-------------------------------------

Ravi Chandra / ravi@chandra.cc / 27-Nov-2012

## Problem ##

Given a 2D maze represented as a matrix of `{ 0, 1 }` where `0` is a wall and `1` is a path, find a path through the maze. Entry is fixed at (x, y) of `(1, 0)` and exits can be on any edge.

Example:

    0,1,0,0,0,0,0,0,0,0,0
    0,1,1,1,1,1,1,0,0,0,0
    0,0,0,1,0,0,1,1,0,0,0
    0,0,1,1,0,0,1,0,0,0,0
    0,0,0,0,0,1,1,1,1,0,0
    0,0,1,1,1,1,0,0,1,0,0
    0,0,0,1,0,0,0,0,0,0,0

*Entry at (1,0)*
 
*Solution: (1,0)(1,1)(2,1)(3,1)(4,1)(5,1)(6,1)(6,2)(6,3)(6,4)(5,4)(5,5)(4,5)(3,5)(3,6)*

#### Assumptions ####

The problem description is light on details so let's make the following assumptions:

1. we should find the **shortest-path**
2. there could be multiple exits and/or loops within the maze

**Note:** my solutions diverge from the original specification as I use `[row][column]` array indexing L-R T-B, rather than `[x][y]` spatial co-ordinates.

## Approach ##

Because we assumed (1) we need the shortest-path we will ignore any naive methods such as random mouse approach and hugging the wall. Both techniques will find a valid path, but it won't be optimal.

Instead let's use two different methods: depth-first search and breadth-first search. Both model the maze as a graph, so each matrix element or path, can be considered a node.

I have submitted two different implementations as I think each one showcases different skills, as mentioned in the Outcomes section below.

##### Python prototypes #####

I have implemented each algorithm using the Python high-level lanaguage to verify the correctness of the algorithm and path-finding concept. This is not something I would always do, but in this case I found it beneficial to experiment with the concepts in a free environment. Python is dynamically-typed and has a very strong library making it great for rapid prototyping.

Within each solution directory will be a python `.py` file that can be run as: `python bfs.py` etc.

##### C implementation #####

The final programs have been implemented using only C. As such I have implemented some of the primitives from scratch, e.g. the linked-list queue. For small problem sizes, a viable alternative would be statically allocated arrays and memory space. A queue is dynamic and results in less memory use at the expense of traversal computation. Mainly, it demonstrates I have the skills necessary to implement linked data structures :-) I am aware of the possibility of leveraging `sys/queue.h` etc but chose not to use this as I am unfamiliar with the test environment.

## Outcomes ##

I believe my submission captures the following skills:

1. rapid prototyping and experimentation using dynamic languages
2. understanding of algorithms and applying computer science theory to problem solving
3. familiar with recursive programming
4. familiar with data structures and modeling them within pure C
5. familiarity with development environment, source control, makefiles etc
6. technical documentation and reporting


# Implementation #
------------------

## Depth-first search ##

We use a depth-first search (DFS) to build a **distance table** between the start point and any other point in the maze. We need to walk the entire maze to build this table, and we may retraverse a node if we have a lower cost (i.e. came from a different, cheaper, path),

Example of the distance table:

    0   1   0   0   0   0   0   0   0   0   0 
    0   2   3   4   5   6   7   0   0   0   0 
    0   0   0   5   0   0   8   9   0   0   0 
    0   0   7   6   0   0   9   0   0   0   0 
    0   0   0   0   0  11  10  11  12   0   0 
    0   0  15  14  13  12   0   0  13   0   0 
    0   0   0  15   0   0   0   0   0   0   0 

Depth-first implementation relies on a stack data structure to hold the accummulation of problem state. We have an 'implicit' stack already, the call stack, which we can leverage through recursion, the main technique is:

`dfs_search(i, j, cur_cost)`

within which there will be recurisve calls for each available walking direction, and the updated cost.


## Breadth-first search ##

Breadth-first search (BFS) is a good fit for this problem because of the uniform weighting. When exploring the maze with work by depth level. This means that as soon as we find any exit, we can be sure it is the shortest-path (or atleast as short as any other path).

This approach relies on the use of a *queue* to maintain a list of the elements to be processed, which expands as we visit more modes. The queue is implemented as a simple linked-list with head (remove) and tail (insert) pointers to model a FIFO queue.

In conjunction we use another datastructure to keep track of the nodes we have visitied and a link to their parent (i.e. to track the *path history*). A nice way to do this would be using an associate array (or hash table) but I have not implemented this functionality due to time constraints.

Due to time pressure, I have used a couple of statically defined arrays to hold the final path etc which could probably improved with the use of an adjacency list of hashmap in a more realistic implementation. (Depending on the problem size, of course!)


# Demonstration #
-----------------

I have added one more maze test case that is more complicated and features multiple-exits:

    0 1 0 0 1 0 0 0 0 0 0 
    0 1 1 1 1 1 1 0 0 0 0 
    0 0 0 1 0 0 1 1 1 1 1 
    0 0 1 1 0 0 1 0 0 0 0 
    0 0 0 1 0 1 1 1 1 0 0 
    0 0 1 1 1 1 0 0 1 0 0 
    0 0 0 1 0 0 0 0 0 0 0 

The distance table which shows the paths clearer is as follows:

    0  1  0  0  6  0  0  0  0  0  0 
    0  2  3  4  5  6  7  0  0  0  0 
    0  0  0  5  0  0  8  9 10 11 12 
    0  0  7  6  0  0  9  0  0  0  0 
    0  0  0  7  0 11 10 11 12  0  0 
    0  0  9  8  9 10  0  0 13  0  0 
    0  0  0  9  0  0  0  0  0  0  0 

The example code can be run using the bash build script as follows:

1) build program(s). Binaries will be stored in `bin/`

`$ make clean && make`

2) run with provided test maze:

`$ bin/dfs` or `$ bin/bfs`

3) run with additional multi-exit maze:

`$ bin/dfs 1` or `$ bin/bfs 1`

Output as follows:

    $ make clean && make
    rm -rf bin build
    mkdir -p bin build
    gcc -O2 -Wall -c dfs/dfs.c -o build/dfs.o
    gcc build/dfs.o -o bin/dfs
    gcc -O2 -Wall -c bfs/bfs.c -o build/bfs.o
    gcc build/bfs.o -o bin/bfs
    
    $ bin/bfs 
    ...
    shortest path:
    (0, 1)
    (1, 1)
    (1, 2)
    (1, 3)
    (1, 4)
    (1, 5)
    (1, 6)
    (2, 6)
    (3, 6)
    (4, 6)
    (4, 5)
    (5, 5)
    (5, 4)
    (5, 3)
    (6, 3)
    
    $ bin/dfs 1
    ...
    shortest path:
    (0, 1)
    (1, 1)
    (1, 2)
    (1, 3)
    (1, 4)
    (0, 4)


#### Platform notes ####

All programs have been verified on OS X 10.8.2 (gcc-4.2) and Ubuntu Linux 10.04 x86_64 (gcc-4.4).