//
//  maze path finder, BFS edition. ravi@chandra.cc 27/11/12.
//
//  using BFS to search the maze until and exit is found. BFS searches by depth
//  level so we can be certain the first exit found will be the shortest path
//  (or equal). as we traverse we keep track of history (positional parents)
//  and use these to work backwards and trace the entire path.
//

#define HEIGHT 7
#define WIDTH 11

#define TRUE 1
#define FALSE 0

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// exit location
int EXIT_I = -1;
int EXIT_J = -1;

// input maze pointer
unsigned char (*INPUT)[WIDTH];

unsigned char REF[][WIDTH] = {
    // provided sample
    { 0,1,0,0,0,0,0,0,0,0,0 },
    { 0,1,1,1,1,1,1,0,0,0,0 },
    { 0,0,0,1,0,0,1,1,0,0,0 },
    { 0,0,1,1,0,0,1,0,0,0,0 },
    { 0,0,0,0,0,1,1,1,1,0,0 },
    { 0,0,1,1,1,1,0,0,1,0,0 },
    { 0,0,0,1,0,0,0,0,0,0,0 },
    
};

unsigned char COMPLEX[][WIDTH] = {
    // more complex, multi-exit test case
    { 0,1,0,0,1,0,0,0,0,0,0 },
    { 0,1,1,1,1,1,1,0,0,0,0 },
    { 0,0,0,1,0,0,1,1,1,1,1 },
    { 0,0,1,1,0,0,1,0,0,0,0 },
    { 0,0,0,1,0,1,1,1,1,0,0 },
    { 0,0,1,1,1,1,0,0,1,0,0 },
    { 0,0,0,1,0,0,0,0,0,0,0 },
    
};


// visited table.. would be better as a linked-list but I'm taking a shortcut
// to save time
unsigned char VISITED[HEIGHT][WIDTH];

// main queue implementation, singly-linked list
struct _queue_type {
    int i;
    int j;
    int c; // depth
    int parent_i;
    int parent_j;
    struct _queue_type* next;
};
typedef struct _queue_type queue_type;

queue_type* q_head = NULL;
queue_type* q_tail = NULL;
int q_len = 0;

// insert at END
queue_type* insert_queue(int i, int j, int c, int parent_i, int parent_j)
{
    queue_type* new = NULL;
    
    new = (queue_type *)malloc(sizeof(queue_type));
    if (new == NULL) printf("failed to malloc! oh noes!");
    
    new->next = NULL;
    new->i = i;
    new->j = j;
    new->c = c;
    new->parent_i = parent_i;
    new->parent_j = parent_j;
    
    // update list pointers
    if (q_len++ == 0)
    {
        // empty list, set head pointer
        q_head = new;
        q_tail = new;
    }
    else
    {
        // link old tail to this element
        q_tail->next = new;
        q_tail = new;
    }
    
    return new;
}

// remove from HEAD
void remove_queue()
{
    if (q_len-- == 0) return;
    
    queue_type* next = q_head->next;
    free(q_head);
    q_head = next;
}

void print_queue()
{
    queue_type* cur = q_head;
    
    if (q_len == 0) return;
    
    do
    {
        printf("(%d, %d)\n", cur->i, cur->j);
        cur = cur->next;
    }
    while (cur != NULL);
}

// visited graph to store the shortest path information
struct _graph_type {
    int i;
    int j;
    struct _graph_type* parent;
    int array_loc; // for forward path calculation
};
typedef struct _graph_type graph_type;

int path_len = 0;
graph_type PATH[100]; // yeah better to make a linked list
int FORWARD_PATH[100]; // a bit of a hack, it's 2AM
int forward_len = 0;

// add node to path array
void insert_graph(int i, int j, graph_type* parent)
{
    PATH[path_len].i = i;
    PATH[path_len].j = j;
    PATH[path_len].parent = parent;
    PATH[path_len].array_loc = path_len;
    ++path_len;
}

// return parent path object given coordinates, this kludge is here because we
// are using a flat 1D array (but of referential graph objects)
graph_type* graph_find_parent(int parent_i, int parent_j)
{
    int i;

    // linear search, yeah
    for (i=0; i<path_len; i++)
    {
        if ((PATH[i].i==parent_i) && (PATH[i].j==parent_j))
            return &PATH[i];
    }
    return NULL;
}

// essentially build the shortest path in reverse for printing
void find_forward_path(graph_type* start)
{
    graph_type* cur = start;
    int i = 0;
    
    do
    {
        FORWARD_PATH[i++] = cur->array_loc;
        cur = cur->parent;
        forward_len++;
    }
    while (cur != NULL);
    FORWARD_PATH[i] = -1;
}

void print_path()
{
    int i = 0;
    graph_type* cur;
    
    // iterate through the path following indicies stored in `forward_path`
    for (i=forward_len-1; i>=0; i--)
    {
        cur = &PATH[FORWARD_PATH[i]];
        printf("(%d, %d)\n", cur->i, cur->j);
    }
}

// core search algorithm
graph_type* search(int start_i, int start_j)
{
    int depth = 1;
    
    // initialise queue with entry path
    insert_queue(start_i, start_j, depth, -1, -1);
    
    while (q_len > 0)
    {
        int i, j, c, parent_i, parent_j;
        
        // pop queue element
        i = q_head->i;
        j = q_head->j;
        c = q_head->c;
        parent_i = q_head->parent_i;
        parent_j = q_head->parent_j;
        remove_queue();
        
        // mark as visited and add to our traversal graph
        VISITED[i][j] = TRUE;
        if (parent_i < 0)
            insert_graph(i, j, NULL);
        else
            insert_graph(i, j, graph_find_parent(parent_i, parent_j));
        
        // check if this is a valid exit
        if (c > 1)
        {
            if (((i==0) || (i==HEIGHT-1)) || ((j==0) || (j==WIDTH-1)))
            {
                printf("exit found!\n");
                EXIT_I = i;
                EXIT_J = j;
                return &PATH[path_len-1];
            }
        }
        
        // add possible unvisited edges into the queue
        if ((i-1>=0) && (INPUT[i-1][j]>0) && (VISITED[i-1][j]==0))
            insert_queue(i-1, j, c+1, i, j); // north
        if ((j-1>=0) && (INPUT[i][j-1]>0) && (VISITED[i][j-1]==0))
            insert_queue(i, j-1, c+1, i, j); // north
        if ((j+1<WIDTH) && (INPUT[i][j+1]>0) && (VISITED[i][j+1]==0))
            insert_queue(i, j+1, c+1, i, j); // north
        if ((i+1<HEIGHT) && (INPUT[i+1][j]>0) && (VISITED[i+1][j]==0))
            insert_queue(i+1, j, c+1, i, j); // north
    }
 
    return NULL;
}


// quick print function for maze visualisation
void print_matrix(unsigned char m[][WIDTH])
{
    int i, j;
    for (i=0; i<HEIGHT; i++)
    {
        for (j=0; j<WIDTH; j++)
            printf("%2d ", m[i][j]);
        printf("\n");
    }
}


int main(int argc, char **argv)
{
    // choose between simple or complex test case
    if (argc > 1) INPUT = COMPLEX;
    else INPUT = REF;

    // initialise distance table
    memset(VISITED, FALSE, WIDTH*HEIGHT);
    
    printf("\ninput maze:\n");
    print_matrix(INPUT);

    // perform breadth-first search and record the shortest path
    find_forward_path(search(0, 1));

    printf("\nvisited tiles:\n");
    print_matrix(VISITED);
 
    printf("\nshortest path:\n");
    print_path();
    
    return 0;
}
