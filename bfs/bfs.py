# breadth first search

from copy import deepcopy

def print_matrix(label, x):
    print "%s:" % label
    for r in x:
        s = ""
        for c in r:
            s += "%3d " % c
        print s
    print

ref = [
[ 0,1,0,0,0,0,0,0,0,0,0 ],
[ 0,1,1,1,1,1,1,0,0,0,0 ],
[ 0,0,0,1,0,0,1,1,0,0,0 ],
[ 0,0,1,1,0,0,1,0,0,0,0 ],
[ 0,0,0,0,0,1,1,1,1,0,0 ],
[ 0,0,1,1,1,1,0,0,1,0,0 ],
[ 0,0,0,1,0,0,0,0,0,0,0 ],
]

ref = [
[ 0,1,0,0,1,0,0,0,0,0,0 ],
[ 0,1,1,1,1,1,1,0,0,0,0 ],
[ 0,0,0,1,0,0,1,1,1,1,1 ],
[ 0,0,1,1,0,0,1,0,0,0,0 ],
[ 0,0,0,1,0,1,1,1,1,0,0 ],
[ 0,0,1,1,1,1,0,0,1,0,0 ],
[ 0,0,0,1,0,0,0,0,0,0,0 ],
]

# store tuple (parent_i, parent_j)
visited = {}

maze = deepcopy(ref)

WIDTH = len(ref[0])
HEIGHT = len(ref)


def search(start_i, start_j):
    c = 1 # depth

    # path queue, seed with start point
    Q = [(start_i, start_j, c, None, None)]
    #visited[(start_i, start_j)] = (None, None)

    while len(Q) > 0:
        i, j, c, parent_i, parent_j = Q.pop(0) # FIFO
        visited[(i,j)] = (parent_i, parent_j)
        print "(%d, %d, %d)" % (i, j, c)

        # check for exit, only if distance is greater than 1
        if c > 1:
            if i in [0, HEIGHT-1] or j in [0, WIDTH-1]:
                print "exit found!"
                return i, j

        # add edges, but don't revisit! find shortest path ONLY
        # north
        if i-1 >= 0 and maze[i-1][j] and (i-1, j) not in visited:
            Q.append((i-1, j, c+1, i, j))
        # west
        if j-1 >= 0 and maze[i][j-1] and (i, j-1) not in visited:
            Q.append((i, j-1, c+1, i, j))
        # east
        if j+1 < WIDTH and maze[i][j+1] and (i, j+1) not in visited:
            Q.append((i, j+1, c+1, i, j))
        # south
        if i+1 < HEIGHT and maze[i+1][j] and (i+1, j) not in visited:
            Q.append((i+1, j, c+1, i, j))


path = []
def path_finder(i, j):
    # append to path chain
    path.append((i, j))

    # iterate backwards looking at parent pointers
    _parent = visited[(i,j)]
    while _parent != (None, None):
        path.append(_parent)
        _parent = visited[_parent]


print_matrix('init', ref)
_exit = search(0, 1)
path_finder(*_exit)
path.reverse()
print path
