//
//  maze path finder, DFS edition. ravi@chandra.cc 27/11/12.
//
//  using DFS to build a distance table of the entire maze, then find lowest
//  cost exit and trace backwards.
//

#define HEIGHT 7
#define WIDTH 11

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


// input maze pointer
unsigned char (*INPUT)[WIDTH];

unsigned char REF[][WIDTH] = {
    // provided sample
    { 0,1,0,0,0,0,0,0,0,0,0 },
    { 0,1,1,1,1,1,1,0,0,0,0 },
    { 0,0,0,1,0,0,1,1,0,0,0 },
    { 0,0,1,1,0,0,1,0,0,0,0 },
    { 0,0,0,0,0,1,1,1,1,0,0 },
    { 0,0,1,1,1,1,0,0,1,0,0 },
    { 0,0,0,1,0,0,0,0,0,0,0 },
};

unsigned char COMPLEX[][WIDTH] = {
    // more complex, multi-exit test case
    { 0,1,0,0,1,0,0,0,0,0,0 },
    { 0,1,1,1,1,1,1,0,0,0,0 },
    { 0,0,0,1,0,0,1,1,1,1,1 },
    { 0,0,1,1,0,0,1,0,0,0,0 },
    { 0,0,0,1,0,1,1,1,1,0,0 },
    { 0,0,1,1,1,1,0,0,1,0,0 },
    { 0,0,0,1,0,0,0,0,0,0,0 },
};

// distance table
unsigned char DIST[HEIGHT][WIDTH];

// list of maze exits
struct _exit_type {
    int i;
    int j;
    int c; // distance
};
typedef struct _exit_type exit_type;
exit_type EXITS[20];
unsigned int num_exits = 0;

// maze path
struct _path_type {
    int i;
    int j;
};
typedef struct _path_type path_type;
path_type PATH[100];
unsigned int path_len = 0;


// quick print function for maze visualisation
void print_matrix(unsigned char m[][WIDTH])
{
    int i, j;
    for (i=0; i<HEIGHT; i++)
    {
        for (j=0; j<WIDTH; j++)
            printf("%2d ", m[i][j]);
        printf("\n");
    }
}

void search(int i, int j, int c, int p_i, int p_j)
{
    // omitting sanity checks for brevity, they're actually covered when we recurse.
    
    // check if we've been here before and it was a better path
    if ((DIST[i][j]>0) && (DIST[i][j]<=c))
    {
        printf("found a worse path :(\n");
        return;
    }
    
    // update distance table for current location
    DIST[i][j] = c;
    
    // check if this is a suitable exit
    if (c > 1)
    {
        if (((i==0) || (i==HEIGHT-1)) || ((j==0) || (j==WIDTH-1)))
        {
            printf("exit found!\n");
            EXITS[num_exits].i = i;
            EXITS[num_exits].j = j;
            EXITS[num_exits].c = c;
            ++num_exits;
            return;
        }
    }
    
    // recurse in all valid directions, checking for boundary conditions, walls,
    // and whether we just came from that location (to avoid a loop).
    
    // north
    if ((i-1>=0) && (INPUT[i-1][j]>0))
    {
        if ((i-1!=p_i) || (j!=p_j)) search(i-1, j, c+1, i, j);
    }
    // west
    if ((j-1>=0) && (INPUT[i][j-1]>0))
    {
        if ((i!=p_i) || (j-1!=p_j)) search(i, j-1, c+1, i, j);
    }
    // east
    if ((j+1<WIDTH) && (INPUT[i][j+1]>0))
    {
        if ((i!=p_i) || (j+1!=p_j)) search(i, j+1, c+1, i, j);
    }
    // south
    if ((i+1<HEIGHT) && (INPUT[i+1][j]>0))
    {
        if ((i+1!=p_i) || (j!=p_j)) search(i+1, j, c+1, i, j);
    }
    
}

int exit_compare(const void *a, const void *b)
{
    return ( ((exit_type*)a)->c - ((exit_type*)b)->c );
}

void build_path(i, j, d)
{
    // append to path chain
    PATH[path_len].i = i;
    PATH[path_len].j = j;
    ++path_len;
    
    // check if we've reached the start
    if (d == 1) return;
    
    // recurse back up looking for previous path
    if ((i-1>=0) && DIST[i-1][j] == d-1) build_path(i-1, j, d-1); // north
    if ((j-1>=0) && DIST[i][j-1] == d-1) build_path(i, j-1, d-1); // west
    if ((j+1<WIDTH) && DIST[i][j+1] == d-1) build_path(i, j+1, d-1); // east
    if ((i+1<HEIGHT) && DIST[i+1][j] == d-1) build_path(i+1, j, d-1); // south
}

void trace_exit()
{
    exit_type *best;
    
    // find fastest exit by quick sort
    qsort(EXITS, num_exits, sizeof(exit_type), exit_compare);
    best = &EXITS[0];
    
    // trace backwards to find best path
    build_path(best->i, best->j, best->c);
    
    // iterate and print shortest path
    do {
        path_type *path;
        path = &PATH[--path_len];
        printf("(%d, %d)\n", path->i, path->j);
    }
    while (path_len != 0);
}


int main(int argc, char **argv)
{
    // choose between simple or complex test case
    if (argc > 1) INPUT = COMPLEX;
    else INPUT = REF;
    
    // initialise distance table
    memset(DIST, 0, WIDTH*HEIGHT);
    
    // clear exits list
    memset(EXITS, 0, 20*sizeof(exit_type));
    
    printf("\ninput maze:\n");
    print_matrix(INPUT);
 
    // build distance table
    search(0, 1, 1, 0, 0);
 
    printf("\ndistance table:\n");
    print_matrix(DIST);
    
    printf("\nshortest path:\n");
    // trace and print shortest path
    trace_exit();

    return 0;
}
