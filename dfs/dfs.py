from copy import deepcopy

def print_matrix(label, x):
    print "%s:" % label
    for r in x:
        s = ""
        for c in r:
            s += "%3d " % c
        print s
    print

ref = [
[ 0,1,0,0,0,0,0,0,0,0,0 ],
[ 0,1,1,1,1,1,1,0,0,0,0 ],
[ 0,0,0,1,0,0,1,1,0,0,0 ],
[ 0,0,1,1,0,0,1,0,0,0,0 ],
[ 0,0,0,0,0,1,1,1,1,0,0 ],
[ 0,0,1,1,1,1,0,0,1,0,0 ],
[ 0,0,0,1,0,0,0,0,0,0,0 ],
]

ref = [
[ 0,1,0,0,1,0,0,0,0,0,0 ],
[ 0,1,1,1,1,1,1,0,0,0,0 ],
[ 0,0,0,1,0,0,1,1,1,1,1 ],
[ 0,0,1,1,0,0,1,0,0,0,0 ],
[ 0,0,0,1,0,1,1,1,1,0,0 ],
[ 0,0,1,1,1,1,0,0,1,0,0 ],
[ 0,0,0,1,0,0,0,0,0,0,0 ],
]

dist = [ [0]*len(ref[0]) for x in xrange(len(ref)) ]

maze = deepcopy(ref)

WIDTH = len(ref[0])
HEIGHT = len(ref)



def search(i, j, c, p_i, p_j):
    if i < 0 or i >= HEIGHT or j < 0 or j >= WIDTH:
        print "failed boundary"
        return
    print "(%d, %d, %d)" % (i,j,c)
    # check if valid path
    if maze[i][j] == 0:
        print "failed path"
        return
    # check if we've been here before!
    if dist[i][j] > 0 and dist[i][j] <= c:
        print "already visited and was lower!"
        return

    dist[i][j] = c

    # check for exit, only if distance is greater than 1
    if c > 1:
        if i in [0, HEIGHT-1] or j in [0, WIDTH-1]:
            print "exit found!"
            return

    # north
    if i-1 >= 0 and maze[i-1][j]:
        if i-1 != p_i or j != p_j:
            search(i-1, j, c+1, i, j)
    # west
    if j-1 >= 0 and maze[i][j-1]:
        if i != p_i or j-1 != p_j:
            search(i, j-1, c+1, i, j)
    # east
    if j+1 < WIDTH and maze[i][j+1]:
        if i != p_i or j+1 != p_j:
            search(i, j+1, c+1, i, j)
    # south
    if i+1 < HEIGHT and maze[i+1][j]:
        if i+1 != p_i or j != p_j:
            search(i+1, j, c+1, i, j)


def find_shortest_exit():
    # find exits, stored as tuple (i, j, dist)
    exits = []
    for i in xrange(HEIGHT):
        for j in xrange(WIDTH):
            if (i > 0 and i < HEIGHT-1) and (j > 0 and j < WIDTH-1):
                continue
            if dist[i][j] > 1:
                exits.append((i, j, dist[i][j]))
    print exits

    # find shortest path (sort on `dist' key)
    return sorted(exits, key=lambda x: x[2])[0]


path = []
def path_finder(i, j, d):
    # append to path chain
    path.append((i, j))

    if d == 1:
        print "start found!"
        return

    # recursively go backwards to trace path
    # north
    if i-1 >= 0 and dist[i-1][j] == d-1:
        path_finder(i-1, j, d-1)
    # west
    if j-1 >= 0 and dist[i][j-1] == d-1:
        path_finder(i, j-1, d-1)
    # east
    if j+1 < WIDTH and dist[i][j+1] == d-1:
        path_finder(i, j+1, d-1)
    # south
    if i+1 < HEIGHT and dist[i+1][j] == d-1:
        path_finder(i+1, j, d-1)


print_matrix('init', ref)
search(0, 1, 1, 0, 0)
print_matrix('dist', dist)
path_finder(*find_shortest_exit())
path.reverse()
print path
